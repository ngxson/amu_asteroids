package fr.univamu.asteroid.game;

import fr.univamu.asteroid.tools.Vector;

import java.util.ArrayList;
import java.util.List;

/**
 * Models a spaceship controlled by a player.
 */
public class Spaceship {

  private static final int INIT_LIFES = 5;
  private static final double MAIN_ENGINE_POWER = 180;
  private static final double TURNING_SPEED = 10;
  public static final int TURN_RIGHT = -1;
  public static final int TURN_LEFT = 1;
  public static final int TURN_STAY = 0;

  /**
   * Count number of lifes
   */
  private int lifes = INIT_LIFES;

  /**
   * Count number of seconds from the beginning of game
   */
  private double timeLapsed = 0;

  /**
   * Turning direction
   */
  private int turningDirection = TURN_STAY;

  /**
   * The position of the center of the spaceship
   */
  private Vector position;

  /**
   * The speed of the spaceship
   */
  private Vector speed = new Vector(0, 0);

  /**
   * The forward direction for the spaceship, encoding the rotation
   * from horizontal of its image and the direction of acceleration.
   */
  private Vector direction = new Vector(1, 0);

  /**
   * Controls if the main engine, with forward acceleration, is powered on.
   */
  private boolean isMainEngineOn = false;


  /**
   * @return the position of the spaceship
   */
  public Vector getPosition() {
    return position;
  }

  /**
   * @return the angle of the spaceship in degree, where 0 is facing right.
   */
  public double getDirection() {
    return direction.angle();
  }


  /**
   * @return whether the main engine is on (forward acceleration).
   */
  public boolean isMainEngineOn() {
    return isMainEngineOn;
  }


  /**
   * Initially the spaceship will be positioned at the center of space.
   */
  public Spaceship() {
    this.position =
      new Vector(
        Space.SPACE_HEIGHT / 2,
        Space.SPACE_WIDTH / 2
      );
  }


  /**
   * The spaceship is a moving object. Every now and then, its position
   * must be updated, as well as other parameters evolving with time. This
   * method simulates the effects of a delay <em>dt</em> over the spaceship.
   * For good accuracy this delay should be kept small.
   *
   * @param dt the time delay to simulate.
   */
  public void update(double dt) {
    timeLapsed += dt;
    speed = speed.add(getAcceleration().multiply(dt));
    position = position.add(speed.multiply(dt));
    updateAngle();
    position = Space.toricRemap(position);
  }

  /**
   * Update angle of the spaceship
   */
  private void updateAngle() {
    direction = direction.rotate(turningDirection * TURNING_SPEED);
  }


  /**
   * Switches the main engine (powering forward acceleration) on.
   */
  public void startMainEngine() {
    isMainEngineOn = true;
  }

  /**
   * Switches the main engine (powering forward acceleration) off.
   */
  public void stopMainEngine() {
    isMainEngineOn = false;
  }

  /**
   * Set rotating engine
   */
  public void setRotateEngine(int direct) {
    turningDirection = direct;
  }

  /**
   * Get rotating engine
   */
  public int getRotateEngine() {
    return turningDirection;
  }


  /**
   * A list of points on the boundary of the spaceship, used
   * to detect collision with other objects.
   */
  private static final List<Vector> contactPoints =
    List.of(
      new Vector(0,0),
      new Vector(27,0),
      new Vector(14.5,1.5),
      new Vector(2,3),
      new Vector(0,18),
      new Vector(-13,18),
      new Vector(-14,2),
      new Vector(-14,-2),
      new Vector(-13,-18),
      new Vector(0,-18),
      new Vector(2,-3),
      new Vector(14.5,-1.5)
    );

  /**
   * Calculate the acceleration
   */
  public Vector getAcceleration() {
    return isMainEngineOn
            ? direction.multiply(MAIN_ENGINE_POWER)
            : new Vector(0, 0);
  }

  /**
   * Check if the spaceship collides with 1 asteroid
   *
   * @param asteroid
   * @return true if spaceship crashed
   */
  public boolean collides(Asteroid asteroid) {
    if (timeLapsed <= 5) return false;
    for (Vector corner : contactPoints) {
      Vector realCornerPosition = corner
              .rotate(getDirection())
              .add(position);
      if (asteroid.contains(realCornerPosition)) return true;
    }
    return false;
  }

  /**
   * Lost a life
   */
  public void lostALife() {
    timeLapsed = 0;
    lifes--;
  }

  /**
   *
   * @return number of lifes
   */
  public int getLives() {
    return lifes;
  }

    /**
     * Make a fire
     * @return a projectile
     */
  public Projectile fire() {
      Projectile mProjectile = new Projectile(
              position.add(direction.multiply(30)),
              speed.add(direction.multiply(300))
      );

      return mProjectile;
  }
}
