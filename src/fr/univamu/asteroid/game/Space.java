package fr.univamu.asteroid.game;


import fr.univamu.asteroid.tools.Vector;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A Space contains all the information determining the current state of
 * the game, and methods implementing how the game state changes, and how
 * the game ends (basically the rules of the game).
 */
public class Space {

  public static final double SPACE_WIDTH = 800;
  public static final double SPACE_HEIGHT = 800;

  public static final int INITIAL_ASTEROID_COUNT = 10;
  public static final double INITIAL_ASTEROID_SIZE = 5;

  private static final double MIN_TIME_BETWEEN_PROJECTILES = 0.2;
  private double lastFireTime = 0;

  /**
   * We don't want asteroids to spawn on the spaceship. This parameter
   * controls how close an asteroid can be from the spaceship initially,
   * in pixels.
   */
  private static final double STARTING_SECURITY_DISTANCE = 80;

  /**
   * An object able to create random items, like asteroids or positions.
   */
  public static final RandomGenerator generator = new RandomGenerator();


  private Spaceship spaceship;
  private List<Asteroid> asteroids;
  private Score score;
  private List<Projectile> projectiles;

  public List<Projectile> getProjectiles() {
    return projectiles;
  }

  private List<Projectile> getDeadProjectiles() {
    List<Projectile> deadProjectiles = new ArrayList<>();
    for (Projectile projectile : projectiles) {
      if (!projectile.isAlive()) {
        deadProjectiles.add(projectile);
      }
    }
    return deadProjectiles;
  }

  private void removeDeadProjectiles() {
    for (Projectile projectile : getDeadProjectiles()) {
      projectiles.remove(projectile);
    }
  }

  public void addProjectile(Projectile projectile) {
    if (lastFireTime <= 0) {
      projectiles.add(projectile);
      lastFireTime = MIN_TIME_BETWEEN_PROJECTILES;
    }
  }

  public Spaceship getSpaceship() {
    return spaceship;
  }

  public List<Asteroid> getAsteroids() {
    return asteroids;
  }

  public Score getScore() {
    return score;
  }

  public Space() {
    spaceship = new Spaceship();
    asteroids = new ArrayList<>(INITIAL_ASTEROID_COUNT);
    for (int i = 0; i < INITIAL_ASTEROID_COUNT; i++) {
      asteroids.add(generateInitialAsteroid());
    }
    projectiles = new ArrayList<>();
    score = new Score();
  }


  public void update(double dt) {
    lastFireTime -= dt;
    for (Asteroid asteroid : asteroids) {
      asteroid.update(dt);
    }
    processProjectiles(dt);
    spaceship.update(dt);
    removeDeadProjectiles();
    score.update(dt);
  }

  private void processProjectiles(double dt) {
    updateProjectiles(dt);
    Set<Asteroid> hittedAsteroids = new HashSet<>();
    Set<Projectile> hittedProjectiles = new HashSet<>();
    findProjectilesHits(hittedAsteroids, hittedProjectiles);
    remove(hittedProjectiles);
    fragment(hittedAsteroids);
  }

  private void fragment(Set<Asteroid> hittedAsteroids) {
    // replace hitted asteroids with smaller one smaller
    for (Asteroid asteroid : hittedAsteroids) {
      score.notifyAsteroidHit();
      Asteroid oldAsteroid = asteroid;
      double newSize = oldAsteroid.getSize() - 1;
      asteroids.remove(oldAsteroid);

      if (newSize <= 0) {
        // if the old asteroid is disappeared, generate a new one
        Asteroid newAsteroid = this.generateInitialAsteroid();
        asteroids.add(newAsteroid);
        // and add score
      } else {
        // if not, replace with new one
        Asteroid newAsteroid = Space.generator.asteroid(
                oldAsteroid.getPosition(),
                newSize
        );
        asteroids.add(newAsteroid);
      }
    }
  }

  private void remove(Set<Projectile> hittedProjectiles) {
    for (Projectile projectile : hittedProjectiles) {
      projectiles.remove(projectile);
    }
  }

  private void findProjectilesHits(Set<Asteroid> hittedAsteroids, Set<Projectile> hittedProjectiles) {
    for (Projectile projectile : projectiles) {
      // get all asteroids and check if they contact with this projectile
      for (Asteroid asteroid : asteroids) {
        if (projectile.isContainsInAsteroid(asteroid)) {
          hittedAsteroids.add(asteroid);
          hittedProjectiles.add(projectile);
        }
      }
    }
  }

  private void updateProjectiles(double dt) {
    for (Projectile projectile : projectiles) {
      // move the projectiles by time step
      projectile.update(dt);
    }
  }


  public boolean isGameOver() {
    if (hasCollision()) {
      spaceship.lostALife();
    }

    return spaceship.getLives() == 0;
  }


  /**
   * Generates a random asteroid with standard parameters, whose distance
   * to the spaceship is large enough.
   *
   * @return a random asteroid
   */
  public Asteroid generateInitialAsteroid() {
    Asteroid asteroid = generator.asteroid(INITIAL_ASTEROID_SIZE);
    double distanceFromSpaceship =
      asteroid.getPosition().distanceTo(spaceship.getPosition());
    if (distanceFromSpaceship < STARTING_SECURITY_DISTANCE) {
      return generateInitialAsteroid();
    }
    return asteroid;
  }


  /**
   * Because the space is toric (things leaving the window on one side
   * reappear on the other side), we need to compute the positions of items
   * leaving the screen to get them back on the other side. This method takes
   * an arbitrary vector and maps it to valid toric coordinates.
   *
   * @param position any position
   * @return the same position with canonical toric coordinates
   */
  public static Vector toricRemap(Vector position) {
    return new Vector(
      clamp(position.getX(), SPACE_WIDTH),
      clamp(position.getY(), SPACE_HEIGHT)
    );
  }

  /**
   * Check if the spaceship collides with any asteroids
   *
   * @return true if spaceship crashed
   */
  public boolean hasCollision() {
    for (Asteroid asteroid : asteroids) {
      if (spaceship.collides(asteroid)) return true;
    }
    return false;
  }

  public int getLives() {
    return spaceship.getLives();
  }


  /**
   * Used by remapPosition to compute coordinates between 0 and a bound.
   *
   * @param value the coordinate to recompute
   * @param bound the maximum value allowed for this coordinate
   * @return the corrected coordinate
   */
  private static double clamp(double value, double bound) {
    return value - Math.floor( value / bound) * bound;
  }
}
