package fr.univamu.asteroid.game;

import fr.univamu.asteroid.tools.Vector;

public class Projectile {
    private Vector position;
    private Vector velocity;
    private double live_time;

    Projectile(Vector position, Vector velocity) {
        this.position = position;
        this.velocity = velocity;
        this.live_time = 10;
    }

    /**
     * @return the position of the Projectile
     */
    public Vector getPosition() {
        return position;
    }

    public void update(double dt) {
        live_time -= dt;
        position = position.add(velocity.multiply(dt));
        position = Space.toricRemap(position);
    }

    public boolean isContainsInAsteroid(Asteroid asteroid) {
        return asteroid.contains(this.position);
    }

    public boolean isAlive() {
        return live_time > 0;
    }
}
