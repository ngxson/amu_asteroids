package fr.univamu.asteroid.game;

public class Score {
    public static final int MULTIPLIER_TIME = 2;
    public static final int INIT_MULTIPLIER = 1;
    public static final int INIT_SCORE = 0;
    private static final double LAMBDA_PENALIZE = 0.01;
    private static final double MIN_LAMBDA_PENALIZE = 0.1;
    private double score;
    private int multiplier;
    private double multiplierTimer;
    private double playTime = 0;

    public double getScore() {
        return score;
    }

    public int getMultiplier() {
        return multiplier;
    }

    public Score() {
        score = INIT_SCORE;
        multiplier = INIT_MULTIPLIER;
        multiplierTimer = MULTIPLIER_TIME;
    }

    public void update(double dt) {
        playTime += dt;
        multiplierTimer = multiplierTimer - dt;
        if (multiplierTimer <= 0) {
            multiplierTimer = MULTIPLIER_TIME;
            multiplier = Math.max(1, multiplier - 1);
        }
    }

    private void addPoints(double points) {
        score += points * multiplier * getMultiplierThroughPlayTime();
    }

    /**
     * TP5 Bonus
     * On voudrait pénaliser les joueurs qui ne progressent pas assez vite.
     * Une façon de faire est de faire en sorte que plusle temps passe, moins le score augmente.
     * On propose donc de multiplier tous les gains de temps par une valeur
     * déterminée par une fonction décroissante du temps, par exemple e−λt
     * @return multiplier
     */
    private double getMultiplierThroughPlayTime() {
        return Math.max(
                MIN_LAMBDA_PENALIZE,
                Math.exp(-LAMBDA_PENALIZE * playTime)
        );
    }

    public void notifyAsteroidHit() {
        addPoints(10);
        addMultiplier(1);
    }

    public void addMultiplier(int multiplier) {
        this.multiplier += multiplier;
        this.multiplierTimer = MULTIPLIER_TIME;
    }
}
