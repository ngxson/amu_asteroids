package fr.univamu.asteroid.viewModel;

import fr.univamu.asteroid.game.*;
import fr.univamu.asteroid.view.View;
import javafx.beans.property.ListProperty;

import java.util.List;

public class ViewModel {

  private Space gameState;
  private View view;

  public ViewModel(Space space, View view) {
    this.gameState = space;
    this.view = view;
  }


  public void tick(double dt) {
    gameState.update(dt);
    view.render();

  }

  public void startSpaceshipMainEngine() {
    gameState.getSpaceship().startMainEngine();
  }

  public void stopSpaceshipMainEngine() {
    gameState.getSpaceship().stopMainEngine();
  }

  public void setSpaceshipRotateEngine(int direction) {
    gameState.getSpaceship().setRotateEngine(direction);
  }


  public boolean isGameOver() {
    return gameState.isGameOver();
  }


  public List<Asteroid> getAsteroids() {
    return gameState.getAsteroids();
  }


  public Spaceship getSpaceship() {
    return gameState.getSpaceship();
  }


  public Score getScore() {
    return gameState.getScore();
  }

  public int getLives() {
    return gameState.getLives();
  }

  public List<Projectile> getProjectiles() {
      return gameState.getProjectiles();
  }

  public void fireSpaceshipGun() {
      gameState.addProjectile(gameState.getSpaceship().fire());
  }
}
