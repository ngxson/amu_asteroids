package fr.univamu.asteroid.game;

import fr.univamu.asteroid.tools.Polygon;
import fr.univamu.asteroid.tools.Vector;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AsteroidTest {

    @org.junit.jupiter.api.Test
    void contains() {
        Polygon shape = new Polygon(
                List.of(new Vector(-5,-5),
                        new Vector(5,-5),
                        new Vector(4,2),
                        new Vector(-6,4)
                )
        );

        Asteroid asteroid = new Asteroid(
                new Vector(40, 15),
                shape,
                new Vector(0, 0),
                90,
                1
        );
        asteroid.update(1);

        assertTrue(asteroid.contains(new Vector(40, 15)));
        assertFalse(asteroid.contains(new Vector(20, 45)));

        // question 12
        assertTrue(asteroid.contains(new Vector(39, 14)));
        assertFalse(asteroid.contains(new Vector(35, 9)));
    }
}